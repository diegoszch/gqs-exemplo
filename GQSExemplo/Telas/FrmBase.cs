using System;

namespace GQSExemplo.Telas
{
    public class FrmBase
    {
        public void EscreverTela(string texto)
        {
            Console.WriteLine(texto);
        }

        public void EscreverTelaPausa(string texto)
        {
            EscreverTela(texto);

            Console.ReadKey();
        }

        public string LerTela()
        {
            return Console.ReadLine();
        }

        public string LerTela(string texto)
        {
            EscreverTela(texto);

            return Console.ReadLine();
        }

        public void LimparTela()
        {
            Console.Clear();
        }
    }
}