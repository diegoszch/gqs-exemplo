using GQSExemplo.Entidades;
using System.Collections.Generic;
using System.Linq;

namespace GQSExemplo.Telas
{
    public class FrmCliente : FrmBase
    {
        public List<Cliente> listagem = new List<Cliente>();

        public FrmCliente()
        {
            listagem.Add(new Cliente("Maria"));
            listagem.Add(new Cliente("João"));
        }

        public void Executar()
        {
            var emExecucao = true;

            do
            {
                LimparTela();

                EscreverTela("-- Cadastro de Clientes --");

                EscreverTela("1 - Listar");
                EscreverTela("2 - Cadastrar");
                EscreverTela("3 - Sair");
                EscreverTela("---");

                var opt = LerTela("Informe a opção desejada");
                switch (opt)
                {
                    case "1":
                        ExibirListagem();
                        break;
                    case "2":
                        ExibirCadastro();
                        break;
                    case "3":
                        emExecucao = false;
                        break;
                    default:
                        EscreverTelaPausa("Opção inválida");
                        break;
                }
            } while (emExecucao);
        }

        public void ExibirListagem()
        {
            var emExecucao = true;

            do
            {
                LimparTela();

                EscreverTela("-- Lista de cliente --");

                for (int i = 0; i < listagem.Count; i++)
                    EscreverTela($"{i} - {listagem[i].Nome}");

                EscreverTela("---");
                EscreverTela("Selecione a opcao desejada:");
                EscreverTela("1 - Editar");
                EscreverTela("2 - Remover");
                EscreverTela("0 - Voltar");

                var opt = LerTela();
                switch (opt)
                {
                    case "1": // Editar
                        EditarCadastro();
                        break;

                    case "2": // Remover                    
                        RemoverCadastro();
                        break;

                    default:
                        emExecucao = false;
                        break;
                }

            } while (emExecucao);
        }

        private bool JaExiste(string nome)
        {
            var existe = listagem.Where(x => x.Nome == nome).FirstOrDefault();

            return (existe != null);
        }

        private void RemoverCadastro()
        {
            var idx = LerTela("Informe o Id do cliente");
            listagem.RemoveAt(int.Parse(idx));
        }

        private void EditarCadastro()
        {
            var idx = LerTela("Informe o Id do cliente");

            bool encontrou = false;
            for (int i = 0; i < listagem.Count; i++)
            {
                if (idx == i.ToString())
                {
                    var nome = LerTela("Informe o novo");
                    if (JaExiste(nome))
                    {
                        EscreverTelaPausa("Este nome ja existe na lista");
                    }
                    else
                    {
                        listagem[i].Nome = nome;
                        EscreverTelaPausa("Registro incluido com sucesso");

                        encontrou = true;
                    }
                }
            }

            if (!encontrou)
                EscreverTelaPausa("Id não encontrado.");
        }

        public void ExibirCadastro()
        {
            LimparTela();

            var nome = LerTela("Informe o nome do cliente:");
            if (JaExiste(nome))
            {
                EscreverTelaPausa("Este nome ja existe na lista");
            }
            else
            {
                listagem.Add(new Cliente(nome));
            }
        }
    }
}