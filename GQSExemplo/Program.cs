﻿using GQSExemplo.Telas;

namespace GQSExemplo
{
    class Program
    {
        static void Main(string[] args)
        {
            var frm = new FrmCliente();
            frm.Executar();
        }
    }
}
